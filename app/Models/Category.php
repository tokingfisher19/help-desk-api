<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    const Status = [
//        'Delete'=>0,
        'Active'=>1,
        'Inactive'=>2
    ];

    protected $table = 'categories';
    protected $primaryKey = 'category_id';

    protected $fillable = [
        'category_name',
        'category_status',
    ];

    public function tickets()
    {
        return $this->hasMany(Ticket::class, 'category_id', 'category_id');
    }
}
