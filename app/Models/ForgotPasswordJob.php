<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ForgotPasswordJob extends Model
{
    protected $table = 'forgot_password_jobs';

    protected $fillable = [
        'email',
        'reset_code',
        'status',
    ];
}
