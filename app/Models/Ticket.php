<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    const Status = [
//        'Delete'=>0,
        'Pending'=>1,
        'Not Assign'=>2,
        'On Process'=>3,
        'Done'=>4,
        'ReOpen'=>5,
        'Active'=>6,
    ];
    protected $table = 'tickets';
    protected $primaryKey = 'ticket_id';

    protected $fillable = [
        'name',
        'email',
        'user_id',
        'category_id',
        'subject',
        'details',
        'ticket_status',
    ];

    private function flip_status(){
        return array_flip(self::Status);
    }

    public function getStatusLabelAttribute(){
        $status = self::flip_status();
        return $status[$this->attributes['ticket_status']];
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function category(){
        return $this->belongsTo(Category::class, 'category_id');
    }
}
