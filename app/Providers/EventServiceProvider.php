<?php

namespace App\Providers;

use App\Events\NewUserRegister;
use App\Listeners\NewUserRegisterListener;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        NewUserRegister::class =>[
            NewUserRegisterListener::class
        ]
    ];
}
