<?php


namespace App\Traits;


use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

trait ApiResponseTrait
{
    public static function allResponse($statusText='Error', $status=Response::HTTP_BAD_REQUEST, $ok=false, $message="",  $data=null, $url=''){

        return response()->json([
            'ok'=>$ok,
            'status'=>$status,
            'statusText'=>ucfirst($statusText),
            'message'=>$message,
            'url'=>$url,
            'data'=>$data,
        ], $status);
    }

    public static function collectionResponse($statusText='Success', $status=200, $collection=null){
        return $collection->additional(
            [
                'statusText'=>ucfirst($statusText),
                'status'=>$status,
                'ok'=>true,
                'message'=>''
            ]);
    }

    public static function singleResponse($data, $statusText='Success', $status=200, $ok = true){
        return response()->json([
            'status'=>$status,
            'statusText'=>ucfirst($statusText),
            'ok'=>$ok,
            'data'=>$data,
        ]);
    }

    public static function ValidationResponse($errors=[], $statusText='Validation', $status=Response::HTTP_NOT_ACCEPTABLE, $ok=false)
    {
        $message = null;
        foreach ($errors as $error){
            if(!empty($error)){
                foreach ($error as $errorItem){
                    $message .=  $errorItem .'<br/> ';
                }
            }
        }
        return response()->json([
            'status'=>$status,
            'statusText'=>ucfirst($statusText),
            'ok'=>$ok,
            'message'=>$message,
        ]);
    }

    public static function makeCollection(Collection $list, Request $request){
        if(!empty($request->per_page)){
            $list = $list->paginate($request->per_page);
        }elseif(!empty($request->page)){
            if(!empty($request->per_page)){
                $list = $list->paginate($request->per_page);
            }else{
                $list = $list->paginate();
            }
        }else {
            if(!empty($request->take)){
                $list = $list->take($request->take)->get();
            }else{
                $list = $list->get();
            }
        }

        return $list;
    }
}
