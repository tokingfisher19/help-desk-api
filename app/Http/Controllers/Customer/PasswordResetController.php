<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\ForgotPasswordJob;
use App\Traits\ApiResponseTrait;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class PasswordResetController extends Controller
{
    public function sendForgotPasswordEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);
        if($validator->passes()){
            try {
                DB::beginTransaction();
                $user = User::where('email', $request->email)->first();
                if(empty($user)){
                    throw new \Exception('Email Address Not Match Any Account', Response::HTTP_NOT_FOUND);
                }

                $reset = ForgotPasswordJob::create([
                    'email'=>$request->email,
                    'reset_code'=> random_int(111111, 999999),
                    'status'=>1,
                ]);

                if(!empty($reset)){
                    DB::commit();
                    return ApiResponseTrait::AllResponse('Success', Response::HTTP_OK, 'Password Reset Email Send Successfully');
                }else{
                    throw new \Exception('Invalid Information', Response::HTTP_BAD_REQUEST);
                }
            }catch (\Exception $ex){
                DB::rollBack();
                return ApiResponseTrait::AllResponse('error', $ex->getCode(), $ex->getMessage());
            }
        }else{
            return ApiResponseTrait::ValidationResponse(array_values($validator->errors()->getMessages()), 'validation', Response::HTTP_NOT_ACCEPTABLE);
        }
    }

    public function password_reset(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
            'reset_code' => ['required'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
        if($validator->passes()){
            try {
                DB::beginTransaction();
                $resetInfo = ForgotPasswordJob::where('email', $request->email)
                    ->where('reset_code', $request->reset_code)
                    ->where('status', 2)->latest()->first();

                if(empty($resetInfo)){
                    throw new \Exception('Invalid Information', Response::HTTP_BAD_REQUEST);
                }
                $email = $request->email;
                $pass = $request->password;
                $user = User::where('email', $email)
                        ->update([
                            'password'=> Hash::make($pass)
                        ]);
                if(!empty($user)){
                    $resetInfo = $resetInfo->update([ 'status'=>0]);
                    if(!empty($resetInfo)){
                        DB::commit();
                        $client = DB::table('oauth_clients')->where('password_client', 1)->first();
                        if(!empty($client)){
                            $formParams = [
                                'form_params' => [
                                    'grant_type' => 'password',
                                    'client_id' => 2,
                                    'client_secret' => $client->secret,
                                    'username' => $email,
                                    'password' => $pass,
                                    'scope' => '',
                                ],
                            ];
                            $http = new Client;
                            $response = $http->post(url('api/v1/oauth/token'), $formParams);
                            $reply = json_decode($response->getBody(), true);
                            return ApiResponseTrait::AllResponse('Success', Response::HTTP_OK, 'Password Reset SuccessFully', $reply);
                    }

                    }else{
                        throw new \Exception('Some Error Found', Response::HTTP_BAD_REQUEST);
                    }

                }else{
                    throw new \Exception('Invalid Information', Response::HTTP_NOT_FOUND);
                }
            }catch (\Exception $ex){
                DB::rollBack();
                return ApiResponseTrait::AllResponse('error', $ex->getCode(), $ex->getMessage());
            }
        }else{
            return ApiResponseTrait::ValidationResponse(array_values($validator->errors()->getMessages()), 'validation', Response::HTTP_NOT_ACCEPTABLE);
        }
    }
}
