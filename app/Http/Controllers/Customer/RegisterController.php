<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponseTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function register_customer(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
        if($validator->passes()){
            try {
                DB::beginTransaction();
                $email = $request->email;
                $pass = random_int(1111111111, 9999999999);
                $user = User::create([
                    'name' => $request->name,
                    'email' => $email,
                    'phoneNo' => $request->phoneNo,
                    'password' => Hash::make($pass),
                    'status' => User::STATUS['UnVerified'], # TODO :: Email Validate => 0
                    'role_id' => User::Role['User'],
                    'verify_token'=> Str::uuid(),
                ]);

                if(!empty($user)){
                    DB::commit();
                    $client = DB::table('oauth_clients')->where('password_client', 1)->first();
                    if(!empty($client)){
                        $formParams = [
                            'form_params' => [
                                'grant_type' => 'password',
                                'client_id' => 2,
                                'client_secret' => $client->secret,
                                'username' => $email,
                                'password' => $pass,
                                'scope' => '',
                            ],
                        ];
                        $http = new Client;
                        $response = $http->post(url('api/v1/oauth/token'), $formParams);
                        $reply = json_decode($response->getBody(), true);

                        // Send Verify email from main server
                        $http->get(url(env('WEB_URL').'/verify/email/'.$user->id));
                        return ApiResponseTrait::AllResponse('Success', Response::HTTP_OK, 'Registration Successful.', $reply);
                    }else{
                        Log::error('OAuth password client not Found');
                        throw new \Exception('Some Error Found', Response::HTTP_BAD_REQUEST);
                    }

                }else{
                    throw new \Exception('Invalid Information', Response::HTTP_NOT_FOUND);
                }
            }catch (\Exception $ex){
                DB::rollBack();
                return ApiResponseTrait::AllResponse('error', $ex->getCode(), $ex->getMessage());
            }
        }else{
            return ApiResponseTrait::ValidationResponse(array_values($validator->errors()->getMessages()), 'validation', Response::HTTP_NOT_ACCEPTABLE);
        }
    }
}
