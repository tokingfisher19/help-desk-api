<?php

namespace App\Http\Controllers\Customer;

use App\User;
use Exception;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function update_details(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name'=>'required|string',
            'email'=>'required|string|email|unique:users,email,'.Auth::user()->id,
        ]);

        if($validation->passes()){
            try {
                DB::beginTransaction();
                $user = Auth::user()->update([
                    'name'=>$request->name,
                    'email'=>$request->email,
                ]);

                if(!empty($user)) {
                    DB::commit();
                    return  ApiResponseTrait::AllResponse('success', Response::HTTP_OK, 'Details Update Successfully');
                } else {
                    throw new Exception('Invalid information', Response::HTTP_BAD_REQUEST);
                }
            }catch (Exception $ex){
                DB::rollBack();
                return  ApiResponseTrait::AllResponse('error', $ex->getCode(), $ex->getMessage());
            }

        }else {
            return ApiResponseTrait::ValidationResponse(array_values($validation->errors()->getMessages()));
        }
    }

    public function change_password(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'password'=>'required|string|min:6|confirmed',
        ]);

        if($validation->passes()){
            try {
                DB::beginTransaction();

                $user = Auth::user()->update([
                    'password'=>Hash::make($request->password),
                ]);

                if (!empty($user)) {
                    DB::commit();
                    return  ApiResponseTrait::AllResponse('success', Response::HTTP_OK, 'Password Updated Successfully');
                } else {
                    throw new Exception('Invalid information', Response::HTTP_BAD_REQUEST);
                }
            }catch (Exception $ex){
                DB::rollBack();
                return  ApiResponseTrait::AllResponse('error', $ex->getCode(), $ex->getMessage());
            }

        }else {
            return ApiResponseTrait::ValidationResponse(array_values($validation->errors()->getMessages()));
        }
    }

}
