<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Traits\ApiResponseTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public function logoutApi()
    {
        if (Auth::check()) {
            $user_id = Auth::guard('api')->id();
            DB::table('oauth_access_tokens')->where('user_id', $user_id)->delete();
//            Auth::guard('api')->logout();
            return ApiResponseTrait::allResponse('success', Response::HTTP_OK, 'You Are Logout');
        }else{
            return ApiResponseTrait::allResponse('error', Response::HTTP_BAD_REQUEST, 'You Are Already Logout');
        }
    }

    public function get_user_info(Request $request)
    {
        $user = User::where('id',  Auth::id())->first();
        if(!empty($user)){
            $data = new UserResource($user);
            return ApiResponseTrait::DataResponse($data, 'success', Response::HTTP_OK);
        }else{
            return ApiResponseTrait::AllResponse('error', Response::HTTP_NOT_FOUND, 'Invalid Information');
        }

    }

    public function update_user_device_token(Request $request)
    {
        if(empty($request->device_token)){
            return ApiResponseTrait::AllResponse('error', Response::HTTP_NOT_FOUND, 'Device Token Not Found');
        }
        $user = User::where('id',  Auth::id())
                ->update([
                    'device_token'=>$request->device_token
                ]);
        if(!empty($user)){
            return ApiResponseTrait::AllResponse('Success', Response::HTTP_OK, 'Device Token Updated');
        }else{
            return ApiResponseTrait::AllResponse('error', Response::HTTP_NOT_FOUND, 'Invalid Information');
        }
    }

}
