<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\ticket\TicketCollection;
use App\Http\Resources\ticket\TicketResource;
use App\Models\Ticket;
use App\Traits\ApiResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Exception;

class TicketController extends Controller
{
    public function index(Request $request)
    {
        $tickets = Ticket::where('ticket_status','!=', 0)->with('user', 'category')->latest();
        $list = ApiResponseTrait::makeCollection($tickets, $request);

        if(!empty($list)){
            $coll = new TicketCollection($list);
            return ApiResponseTrait::collectionResponse('Success', Response::HTTP_OK, $coll);
        }else{
            return ApiResponseTrait::allResponse('Error', Response::HTTP_NOT_FOUND, false, 'Tickets Not Found');
        }
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name'=>'required|string',
            'email'=>'required|string',
            'categoryId'=>'required',
            'subject'=>'required|string',
            'details'=>'required|string',
        ]);

        if($validation->passes()){
            try {
                DB::beginTransaction();
                $ticket = Ticket::create([
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'user_id'=>(Auth::user())? Auth::id(): null,
                    'category_id'=>$request->categoryId,
                    'subject'=>$request->subject,
                    'details'=>$request->details,
                    'ticket_status'=>Ticket::Status['Pending'],
                ]);

                if(!empty($ticket)) {
                    DB::commit();
                    return  ApiResponseTrait::AllResponse('success', Response::HTTP_OK, true,'Ticket Created Successfully');
                } else {
                    throw new Exception('Invalid information', Response::HTTP_BAD_REQUEST);
                }
            }catch (Exception $ex){
                DB::rollBack();
                return  ApiResponseTrait::AllResponse('error', $ex->getCode(),false, $ex->getMessage());
            }

        }else {
            return ApiResponseTrait::ValidationResponse(array_values($validation->errors()->getMessages()));
        }
    }

    public function show($id)
    {
        $ticket = Ticket::where('ticket_id', $id)->first();

        if(!empty($ticket)){
            $data = new TicketResource($ticket);
            return ApiResponseTrait::singleResponse($data, 'Success', Response::HTTP_OK, true);
        }
        return ApiResponseTrait::allResponse('Not Found', Response::HTTP_NOT_FOUND, false, 'Ticket Not Found');
    }

    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[
            'name'=>'required|string',
            'email'=>'required|string',
            'categoryId'=>'required',
            'subject'=>'required|string',
            'details'=>'required|string',
        ]);

        if($validation->passes()){
            try {
                DB::beginTransaction();
                $ticket = Ticket::where('ticket_id', $id)->first();
                if(empty($ticket)){
                    throw new Exception('Invalid Ticket Information', Response::HTTP_BAD_REQUEST);
                }

                $ticket = $ticket->update([
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'user_id'=>(Auth::user())? Auth::id(): null,
                    'category_id'=>$request->categoryId,
                    'subject'=>$request->subject,
                    'details'=>$request->details,
                ]);

                if(!empty($ticket)) {
                    DB::commit();
                    return  ApiResponseTrait::AllResponse('success', Response::HTTP_OK, true,'Ticket Updated Successfully');
                } else {
                    throw new Exception('Invalid information', Response::HTTP_BAD_REQUEST);
                }
            }catch (Exception $ex){
                DB::rollBack();
                return  ApiResponseTrait::AllResponse('error', $ex->getCode(),false, $ex->getMessage());
            }

        }else {
            return ApiResponseTrait::ValidationResponse(array_values($validation->errors()->getMessages()));
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $ticket = Ticket::where('ticket_id', $id)->first();
            if(empty($ticket)){
                throw new Exception('Invalid Ticket Information', Response::HTTP_BAD_REQUEST);
            }

            $ticket = $ticket->update([
                'ticket_status'=>0
            ]);

            if(!empty($ticket)) {
                DB::commit();
                return  ApiResponseTrait::AllResponse('success', Response::HTTP_OK, true,'Ticket Deleted Successfully');
            } else {
                throw new Exception('Invalid information', Response::HTTP_BAD_REQUEST);
            }
        }catch (Exception $ex){
            DB::rollBack();
            return  ApiResponseTrait::AllResponse('error', $ex->getCode(),false, $ex->getMessage());
        }
    }
}
