<?php

namespace App\Http\Controllers;

use App\Models\Area;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function logoutApi()
    {
        if (Auth::guard('api')->check()) {
            $user_id = Auth::guard('api')->id();
            DB::table('oauth_access_tokens')->where('user_id', $user_id)->delete();
//            Auth::guard('api')->logout();
            return ApiResponse::allResponse('success', Response::HTTP_OK, 'You Are Logout');
        }else{
            return ApiResponse::allResponse('error', Response::HTTP_BAD_REQUEST, 'You Are Already Logout');
        }
    }

    public function add_area(Request $request)
    {
        $aa = Area::create([
            'area_name'=>$request->name,
            'area_lat'=>$request->lat,
            'area_lng'=>$request->lng,
            'area_status'=>1,
            'service_distance'=>$request->distance,
        ]);

        if($aa){
            return response()->json([
                'status'=>'success',
                'message'=>'done',
                'data'=>$request->all(),
            ]);
        }else{
            return response()->json([
                'status'=>'error',
                'message'=>'Not',
            ]);
        }

    }
}
