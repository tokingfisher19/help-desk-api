<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\category\CategoryCollection;
use App\Http\Resources\category\CategoryResource;
use App\Models\Category;
use App\Traits\ApiResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Exception;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $categories = Category::where('category_status', '!=', 0)->latest();
        $list = ApiResponseTrait::makeCollection($categories, $request);

        if(!empty($list)){
            $coll = new CategoryCollection($list);
            return ApiResponseTrait::collectionResponse('Success', Response::HTTP_OK, $coll);
        }else{
            return ApiResponseTrait::allResponse('Error', Response::HTTP_NOT_FOUND, false, 'No Category Found');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name'=>'required|string',
            'status'=>'required',
        ]);

        if($validation->passes()){
            try {
                DB::beginTransaction();
                $category = Category::create([
                    'category_name'=>$request->name,
                    'category_status'=>(!empty($request->status))?? Category::Status['Inactive']
                ]);

                if(!empty($category)) {
                    DB::commit();
                    return  ApiResponseTrait::AllResponse('success', Response::HTTP_OK, true,'Category Created Successfully');
                } else {
                    throw new Exception('Invalid information', Response::HTTP_BAD_REQUEST);
                }
            }catch (Exception $ex){
                DB::rollBack();
                return  ApiResponseTrait::AllResponse('error', $ex->getCode(),false, $ex->getMessage());
            }

        }else {
            return ApiResponseTrait::ValidationResponse(array_values($validation->errors()->getMessages()));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $category = Category::where('category_id', $id)->with('tickets.user')->first();
        if(!empty($category)){
            $data = new CategoryResource($category);
            return ApiResponseTrait::singleResponse($data, 'Success', Response::HTTP_OK, true);
        }else {
            return ApiResponseTrait::allResponse('Not Found', Response::HTTP_NOT_FOUND, false, 'Category Information Not Found');
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[
            'name'=>'required|string',
            'status'=>'required',
        ]);

        if($validation->passes()){
            try {
                DB::beginTransaction();

                $category = Category::where('category_id', $id)->first();

                if(empty($category)){
                    throw new Exception('Invalid Category Information', Response::HTTP_NOT_FOUND);
                }

                $category = $category->update([
                    'category_name'=>$request->name,
                    'category_status'=>(!empty($request->status))?? Category::Status['Inactive']
                ]);

                if(!empty($category)) {
                    DB::commit();
                    return  ApiResponseTrait::AllResponse('success', Response::HTTP_OK, true,'Category Update Successfully');
                } else {
                    throw new Exception('Invalid information', Response::HTTP_BAD_REQUEST);
                }
            }catch (Exception $ex){
                DB::rollBack();
                return  ApiResponseTrait::AllResponse('error', $ex->getCode(),false, $ex->getMessage());
            }

        }else {
            return ApiResponseTrait::ValidationResponse(array_values($validation->errors()->getMessages()));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();

            $category = Category::where('category_id', $id)->first();

            if(empty($category)){
                throw new Exception('Invalid Category Information', Response::HTTP_NOT_FOUND);
            }

            $category = $category->update([
                'category_status'=>0
            ]);

            if(!empty($category)) {
                DB::commit();
                return  ApiResponseTrait::AllResponse('success', Response::HTTP_OK, true,'Category Deleted Successfully');
            } else {
                throw new Exception('Invalid information', Response::HTTP_BAD_REQUEST);
            }
        }catch (Exception $ex){
            DB::rollBack();
            return  ApiResponseTrait::AllResponse('error', $ex->getCode(),false, $ex->getMessage());
        }
    }
}
