<?php

namespace App\Http\Resources\ticket;

use App\Http\Resources\category\CategoryResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\Resource;

class TicketResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->ticket_id,
            'name'=>$this->name,
            'email'=> $this->email,
            'userId'=> $this->user_id,
            'categoryId'=> $this->category_id,
            'subject'=>$this->subject,
            'description'=>$this->details,
            'status'=>$this->ticket_status,
            'statusLabel'=> $this->status_label,
            'category'=> new CategoryResource($this->whenLoaded('category')),
            'user'=> new UserResource($this->whenLoaded('user')),
        ];
    }
}
