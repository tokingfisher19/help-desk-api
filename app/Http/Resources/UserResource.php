<?php

namespace App\Http\Resources;

use App\Http\Resources\ticket\TicketCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'email'=>$this->email,
            'phoneNo'=>$this->phoneNo,
            'role'=>$this->role,
            'roleLabel'=>$this->role_label,
            'status'=>$this->status,
            'verified'=> ($this->status == 3)? 0 : 1,
            'deviceToken'=> $this->device_token,
            'tickets' => new TicketCollection($this->whenLoaded('tickets'))
        ];
    }
}
