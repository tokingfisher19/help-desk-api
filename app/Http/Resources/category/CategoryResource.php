<?php

namespace App\Http\Resources\category;

use App\Http\Resources\ticket\TicketCollection;
use Illuminate\Http\Resources\Json\Resource;

class CategoryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->category_id,
            'name'=>$this->category_name,
            'status'=> $this->category_status,
            'statusLabel'=> $this->status_label,
            'tickets'=> new TicketCollection($this->whenLoaded('tickets')),
        ];
    }
}
