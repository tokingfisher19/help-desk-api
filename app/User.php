<?php

namespace App;

use App\Models\PurchaseDetail;
use App\Models\QrCode;
use App\Models\Ticket;
use App\Models\VoucherMerchant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    const STATUS = [
        'Active'=>1,
        'Block'=>2,
        'UnVerified'=>3
    ];

    const Role = [
        'Admin'=> 1,
        'User'=>2,
        'Support'=>3,
    ];

    protected $table = 'users';
    protected $primaryKey = 'user_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phoneNo',
        'password',
        'role',
        'status',
        'avatar',
        'verify_token',
        'device_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','verify_token',
    ];

    protected $appends = ['roleLabel'];

    public function getRoleLabelAttribute()
    {
        $role = array_flip(self::Role);
        return $role[$this->attributes['role']];
    }
    public function tickets()
    {
        return $this->hasMany(Ticket::class, 'ticket_id');
    }
}
